USE [master]
GO
/****** Object:  Database [Pokemon]    Script Date: 21/06/2021 4:00:27 p. m. ******/
CREATE DATABASE [Pokemon]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Pokemon', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Pokemon.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Pokemon_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Pokemon_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Pokemon] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Pokemon].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Pokemon] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Pokemon] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Pokemon] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Pokemon] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Pokemon] SET ARITHABORT OFF 
GO
ALTER DATABASE [Pokemon] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [Pokemon] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Pokemon] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Pokemon] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Pokemon] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Pokemon] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Pokemon] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Pokemon] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Pokemon] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Pokemon] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Pokemon] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Pokemon] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Pokemon] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Pokemon] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Pokemon] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Pokemon] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [Pokemon] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Pokemon] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Pokemon] SET  MULTI_USER 
GO
ALTER DATABASE [Pokemon] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Pokemon] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Pokemon] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Pokemon] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Pokemon] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Pokemon] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [Pokemon] SET QUERY_STORE = OFF
GO
USE [Pokemon]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 21/06/2021 4:00:27 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Abilities]    Script Date: 21/06/2021 4:00:27 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Abilities](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdCard] [nvarchar](max) NULL,
	[Name] [nvarchar](max) NULL,
	[Text] [nvarchar](max) NULL,
	[Type] [nvarchar](max) NULL,
 CONSTRAINT [PK_Abilities] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AncientTraits]    Script Date: 21/06/2021 4:00:27 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AncientTraits](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdCard] [nvarchar](max) NULL,
	[Name] [nvarchar](max) NULL,
	[Text] [nvarchar](max) NULL,
 CONSTRAINT [PK_AncientTraits] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Attacks]    Script Date: 21/06/2021 4:00:27 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Attacks](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdCard] [nvarchar](max) NULL,
	[Name] [nvarchar](max) NULL,
	[Text] [nvarchar](max) NULL,
	[Damage] [nvarchar](max) NULL,
	[ConvertedEnergyCost] [int] NOT NULL,
 CONSTRAINT [PK_Attacks] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cards]    Script Date: 21/06/2021 4:00:27 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cards](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[NationalPokedexNumber] [int] NOT NULL,
	[ImageUrl] [nvarchar](max) NULL,
	[ImageUrlHiRes] [nvarchar](max) NULL,
	[SuperType] [nvarchar](max) NULL,
	[SubType] [nvarchar](max) NULL,
	[EvolvesFrom] [nvarchar](max) NULL,
	[Hp] [nvarchar](max) NULL,
	[Number] [nvarchar](max) NULL,
	[Artist] [nvarchar](max) NULL,
	[Rarity] [nvarchar](max) NULL,
	[Series] [nvarchar](max) NULL,
	[Set] [nvarchar](max) NULL,
	[SetCode] [nvarchar](max) NULL,
	[ConvertedRetreatCost] [int] NOT NULL,
 CONSTRAINT [PK_Cards] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Resistances]    Script Date: 21/06/2021 4:00:27 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Resistances](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdCard] [nvarchar](max) NULL,
	[Type] [nvarchar](max) NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_Resistances] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Weaknesses]    Script Date: 21/06/2021 4:00:27 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Weaknesses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdCard] [nvarchar](max) NULL,
	[Type] [nvarchar](max) NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_Weaknesses] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [Pokemon] SET  READ_WRITE 
GO
