﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PokemonTCG
{
    public class ConnectionService
    {
        public static string PokemonConnectionString = "";
        public static string SetPokemonConnectionString(IConfiguration config)
        {
            PokemonConnectionString = config.GetConnectionString("PokemonConnectionString");
            return PokemonConnectionString;
        }
    }
}
