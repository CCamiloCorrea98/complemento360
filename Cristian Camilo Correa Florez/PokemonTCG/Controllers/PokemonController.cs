﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PokemonTCG.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace PokemonTCG.Controllers
{
    public class PokemonController : Controller
    {
        private NLog.Logger _Logger;
        private string _IdLog;
        private List<Card> ObjCard;
        private ApiPokemonModelDetail ObjCardDetail;
        private readonly PokemonContext _Context;

        public PokemonController(PokemonContext Context)
        {
            _Logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            _IdLog = $"{DateTime.Now:yyyyMMddhhmmssff} - ";
            _Context = Context;
        }

        public IActionResult Index(string Orden)
       {
            try
            {
                GetAllApiPokemon();

                ViewBag.Id = String.IsNullOrEmpty(Orden) ? "Id_Desc" : "";

                var PokemonOrden = from s in ObjCard
                          select s;

                switch (Orden)
                {
                    case "Id_Desc":
                        _Logger.Debug($"{_IdLog}Se va a ordenar la lista descendientemente");
                        PokemonOrden = PokemonOrden.OrderByDescending(x => x.Id);
                        break;
                    default:
                        _Logger.Debug($"{_IdLog}Se va a ordenar la lista ascendientemente");
                        PokemonOrden = PokemonOrden.OrderBy(x => x.Id);
                        break;
                }
                _Logger.Debug($"{_IdLog}Se retorna la vista index");
                return View(PokemonOrden);
            }
            catch (Exception ex)
            {
                _Logger.Error($"{_IdLog}Exception Index {ex.Message}");
                return View();
            }
            
        }
        public IActionResult Insert(string id)
        {
            try
            {
                GetDetailApiPokemon(id);
                if (IdExistInCard(id) == false)
                {
                    if (InsertPokemonCard(ObjCardDetail.card).Result)
                    {
                        return View(ObjCardDetail.card);
                    }
                    else
                    {
                        return View("Error");
                    }
                }
                else
                {
                    return View("PokemonExist",ObjCardDetail.card);
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"{_IdLog}Exception Insert {ex.Message}");
                return View();
            }
        }
        public IActionResult DetailBd()
        {
            try
            {
                var ObjConsultBd = _Context.Cards.ToList();
                List<Card> ListCardBd = new List<Card>();
                foreach (var item in ObjConsultBd)
                {
                        GetDetailApiPokemon(item.Id);
                        ListCardBd.Add(ObjCardDetail.card);
                }

                var PokemonOrden = from s in ListCardBd
                                   select s;

                return View(PokemonOrden);
            }
            catch (Exception ex)
            {
                _Logger.Error($"{_IdLog}Exception DetailBd {ex.Message}");
                return View("Error");
            }
        }
        public IActionResult Delete(string id)
        {
            try
            {
                var ObjConsultBd = _Context.Cards.Where(x => x.Id == id).FirstOrDefault();
                _Context.Cards.Remove(ObjConsultBd);
                _Context.SaveChanges();

                GetDetailApiPokemon(ObjConsultBd.Id);
                return View(ObjCardDetail.card);
            }
            catch (Exception ex)
            {
                _Logger.Error($"{_IdLog}Exception Delete {ex.Message}");
                return View("Error");
            }

        }

        [HttpPost("InsertPokemon")]
        public async Task<bool> InsertPokemonCard(Card ObjCardInser)
        {
            try
            {
                _Logger.Debug($"{_IdLog}Ingresó al método InsertPokemonCard con el Id del Pokemon {ObjCardInser.Id}");
                using (var transaccion = _Context.Database.BeginTransaction())
                {
                    try
                    {
                        _Logger.Debug($"{_IdLog}Se va a insertar en la tabla Card");
                        _Context.Cards.Add(ObjCardInser);
                        await _Context.SaveChangesAsync();

                        if (ObjCardInser.Id != "")
                        {
                            _Logger.Debug($"{_IdLog}Se insertó correctamente el Pokemon Id {ObjCardInser.Id} a la tabla Card");

                            if (ObjCardInser.Ability != null)
                            {
                                Ability ObjAbility = new Ability();
                                ObjAbility = ObjCardInser.Ability;
                                ObjAbility.IdCard = ObjCardInser.Id;

                                _Logger.Debug($"{_IdLog}Se va a insertar en la tabla Ability");
                                _Context.Abilities.Add(ObjAbility);
                                await _Context.SaveChangesAsync();

                                if (ObjAbility.Id > 0)
                                {
                                    _Logger.Debug($"{_IdLog}Se insertó correctamente Ability del Pokemon Id {ObjCardInser.Id} a la tabla Ability");

                                }
                            }

                            if (ObjCardInser.AncientTrait != null)
                            {
                                Ancienttrait ObjAncientTrait = new Ancienttrait();
                                ObjAncientTrait = ObjCardInser.AncientTrait;
                                ObjAncientTrait.IdCard = ObjCardInser.Id;

                                _Logger.Debug($"{_IdLog}Se va a insertar en la tabla Ancienttrait");
                                _Context.AncientTraits.Add(ObjAncientTrait);
                                await _Context.SaveChangesAsync();

                                if (ObjAncientTrait.Id > 0)
                                {
                                    _Logger.Debug($"{_IdLog}Se insertó correctamente AncientTrait del Pokemon Id {ObjCardInser.Id} a la tabla AncientTrait");

                                }
                            }

                            if (ObjCardInser.Attacks != null && ObjCardInser.Attacks.Length > 0)
                            {
                                foreach (var item in ObjCardInser.Attacks)
                                {
                                    Attack ObjAttack = new Attack();
                                    ObjAttack = item;
                                    ObjAttack.IdCard = ObjCardInser.Id;

                                    _Logger.Debug($"{_IdLog}Se va a insertar en la tabla Attack");
                                    _Context.Attacks.Add(ObjAttack);
                                    await _Context.SaveChangesAsync();

                                    if (ObjAttack.Id > 0)
                                    {
                                        _Logger.Debug($"{_IdLog}Se insertó correctamente Attack del Pokemon Id {ObjCardInser.Id} a la tabla Attack");

                                    }
                                }

                            }

                            if (ObjCardInser.Resistances != null)
                            {
                                foreach (var item in ObjCardInser.Resistances)
                                {
                                    Resistance ObjResistance = new Resistance();
                                    ObjResistance = item;
                                    ObjResistance.IdCard = ObjCardInser.Id;

                                    _Logger.Debug($"{_IdLog}Se va a insertar en la tabla Resistance");
                                    _Context.Resistances.Add(ObjResistance);
                                    await _Context.SaveChangesAsync();

                                    if (ObjResistance.Id > 0)
                                    {
                                        _Logger.Debug($"{_IdLog}Se insertó correctamente Resistance del Pokemon Id {ObjCardInser.Id} a la tabla Resistance");

                                    }
                                }
                            }

                            if (ObjCardInser.Weaknesses != null)
                            {
                                foreach (var item in ObjCardInser.Weaknesses)
                                {
                                    Weakness ObjWeakness = new Weakness();
                                    ObjWeakness = item;
                                    ObjWeakness.IdCard = ObjCardInser.Id;

                                    _Logger.Debug($"{_IdLog}Se va a insertar en la tabla Weakness");
                                    _Context.Weaknesses.Add(ObjWeakness);
                                    await _Context.SaveChangesAsync();

                                    if (ObjWeakness.Id > 0)
                                    {
                                        _Logger.Debug($"{_IdLog}Se insertó correctamente Weakness del Pokemon Id {ObjCardInser.Id} a la tabla Weakness");
                                    }
                                }
                            }
                            transaccion.Commit();
                            return true;
                        }
                    }
                    catch(Exception ex)
                    {
                        transaccion.Rollback();
                        _Logger.Error($"{_IdLog}Exception InsertPokemonCard {ex.Message}");
                        _Logger.Debug($"{_IdLog}Se realiza RollBack de la transacción para el Pokemon id {ObjCardInser.Id}");
                        return false;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                _Logger.Error($"{_IdLog}Exception InsertPokemonCard {ex.Message}");
                return false;
            }
        }
        private void GetAllApiPokemon()
        {
            try
            {
                var client = new HttpClient();
                _Logger.Debug($"{_IdLog}Se va a consultar la api de Pokemon");
                if (ObjCard == null)
                {
                    var json = client.GetStringAsync("https://api.pokemontcg.io/v1/cards");
                    _Logger.Debug($"{_IdLog}Se consultó correctamente");
                    ApiPokemonModel contestjson = JsonConvert.DeserializeObject<ApiPokemonModel>(json.Result);
                    ObjCard = contestjson.cards.Cast<Card>().ToList();
                }
            }
            catch (Exception ex)
            {
                _Logger.Error($"{_IdLog}Exception GetAllApiPokemon {ex.Message}");
            }
        }
        private void GetDetailApiPokemon(string id)
        {
            try
            {
                ObjCardDetail = null;
                var client = new HttpClient();
                _Logger.Debug($"{_IdLog}Se va a consultar la api de Pokemon por el id {id}");
                var json = client.GetStringAsync($"https://api.pokemontcg.io/v1/cards/{id}");
                _Logger.Debug($"{_IdLog}Se consultó correctamente");
                ObjCardDetail = JsonConvert.DeserializeObject<ApiPokemonModelDetail>(json.Result);
            }
            catch (Exception ex)
            {
                _Logger.Error($"{_IdLog}Exception GetDetailApiPokemon {ex.Message}");
            }
        }
        private bool IdExistInCard(string Id)
        {
            return _Context.Cards.Any(x => x.Id == Id);
        }
    }
}