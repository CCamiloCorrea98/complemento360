﻿using Microsoft.EntityFrameworkCore;

namespace PokemonTCG.Models
{
    public class PokemonContext : DbContext
    {
        public PokemonContext()
        {
        }

        public PokemonContext(DbContextOptions<PokemonContext> options)
            : base(options)
        {
        }
        public DbSet<Card> Cards { get; set; }
        public DbSet<Attack> Attacks { get; set; }
        public DbSet<Weakness> Weaknesses { get; set; }
        public DbSet<Resistance> Resistances { get; set; }
        public DbSet<Ability> Abilities { get; set; }
        public DbSet<Ancienttrait> AncientTraits { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConnectionService.PokemonConnectionString);
        }
    }
}
