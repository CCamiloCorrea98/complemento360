﻿using System.ComponentModel.DataAnnotations.Schema;

namespace PokemonTCG.Models
{
    public class ApiPokemonModel
    {
        public Card[] cards { get; set; }
    }
    public class ApiPokemonModelDetail
    {
        public Card card { get; set; }
    }

    public partial class Card
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int NationalPokedexNumber { get; set; }
        public string ImageUrl { get; set; }
        public string ImageUrlHiRes { get; set; }
        [NotMapped]
        public string[] Types { get; set; }
        public string SuperType { get; set; }
        public string SubType { get; set; }
        public string EvolvesFrom { get; set; }
        public string Hp { get; set; }
        public string Number { get; set; }
        public string Artist { get; set; }
        public string Rarity { get; set; }
        public string Series { get; set; }
        public string Set { get; set; }
        public string SetCode { get; set; }
        [NotMapped]
        public Attack[] Attacks { get; set; }
        [NotMapped]
        public Weakness[] Weaknesses { get; set; }
        [NotMapped]
        public string[] RetreatCost { get; set; }
        public int ConvertedRetreatCost { get; set; }
        [NotMapped]
        public Resistance[] Resistances { get; set; }
        [NotMapped]
        public string[] Text { get; set; }
        [NotMapped]
        public Ability Ability { get; set; }
        [NotMapped]
        public Ancienttrait AncientTrait { get; set; }
    }
    public partial class Ability
    {
        public int Id { get; set; }
        public string IdCard { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public string Type { get; set; }
    }
    public partial class Ancienttrait
    {
        public int Id { get; set; }
        public string IdCard { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
    }
    public partial class Attack
    {
        public int Id { get; set; }
        public string IdCard { get; set; }
        [NotMapped]
        public string[] Cost { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public string Damage { get; set; }
        public int ConvertedEnergyCost { get; set; }
    }
    public partial class Weakness
    {
        public int Id { get; set; }
        public string IdCard { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
    }
    public partial class Resistance
    {
        public int Id { get; set; }
        public string IdCard { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
    }

}